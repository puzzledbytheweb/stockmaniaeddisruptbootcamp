import React from "react";
import Button from "../Button/Button";

class SearchStock extends React.Component {
    state = {
        inputValue: ""
    };

    handleChange = value => {
        this.setState({ inputValue: value });
    };

    render() {
        return (
            <form
                onSubmit={event => {
                    event.preventDefault();

                    this.props.fetchStock(this.state.inputValue);
                }}
            >
                <input
                    name="search"
                    onChange={event => this.handleChange(event.target.value)}
                    value={this.state.inputValue}
                />
                <Button>Search!!</Button>
            </form>
        );
    }
}

export default SearchStock;
