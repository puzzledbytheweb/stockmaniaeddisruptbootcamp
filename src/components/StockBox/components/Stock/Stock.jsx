import React from "react";

import Button from "../../../Button/Button";

function Stock(props) {
    return (
        <div>
            {props.children}
            <Button
                handleClick={function() {
                    props.deleteStock(props.index);
                }}
            >
                Delete
            </Button>
        </div>
    );
}

export default Stock;
