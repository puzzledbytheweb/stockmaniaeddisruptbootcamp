import React from "react";
import Stock from "./components/Stock/Stock";

function StockBox(props) {
    return (
        <div>
            {props.stocks.map((stock, index) => (
                <Stock
                    key={index}
                    index={index}
                    deleteStock={props.deleteStock}
                >
                    {stock.symbol} {stock.price}€
                </Stock>
            ))}
        </div>
    );
}

export default StockBox;
