import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";

import Title from "./components/Title/Title";
import SearchStock from "./components/SearchStock/SearchStock";
import StockBox from "./components/StockBox/StockBox";

class App extends Component {
    state = {
        stocks: [],
        isLoading: false,
        error: false
    };

    fetchStock = stockSymbol => {
        this.setState({ isLoading: true }, () => {
            fetch(
                `https://api.iextrading.com/1.0/tops/last?symbols=${stockSymbol}`
            )
                .then(response => response.json())
                .then(json => this.addStock(json[0]))
                .catch(err => this.setState({ error: true, isLoading: false }));
        });
    };

    addStock = stock => {
        // COPY stocks array
        const newStocks = this.state.stocks.slice();

        // Push new Item
        newStocks.push({ symbol: stock.symbol, price: stock.price });

        // Change State
        this.setState({ stocks: newStocks, isLoading: false, error: false });
    };

    deleteStock = index => {
        // COPY stocks array
        const newStocks = this.state.stocks.slice();

        // Delete Item
        newStocks.splice(index, 1);

        // Change State
        this.setState({ stocks: newStocks });
    };

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <Title>StockMania</Title>
                    <h3
                        style={
                            this.state.isLoading
                                ? { display: "block" }
                                : { display: "none" }
                        }
                    >
                        Loading!!!!
                    </h3>
                    <h3
                        style={
                            this.state.error
                                ? { display: "block", color: "red" }
                                : { display: "none" }
                        }
                    >
                        Error!!
                    </h3>
                    <SearchStock fetchStock={this.fetchStock} />
                    <StockBox
                        deleteStock={this.deleteStock}
                        stocks={this.state.stocks}
                    />
                </header>
            </div>
        );
    }
}

export default App;
